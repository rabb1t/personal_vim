#! /bin/bash

echo "Installing Vundle Plugin Manager"
echo ""

sleep 2

git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim

echo "Installing Pathogen Plugin Manager"
echo "" 

sleep 2

cd ~/.vim/bundle && git clone https://github.com/tpope/vim-sensible.git


echo "Moving vimrc to home directory"
echo ""
sleep 2

echo "Installing Plugins"
echo ""
sleep 2

# Syntastic
cd ~/.vim/bundle && \
git clone --depth=1 https://github.com/vim-syntastic/syntastic.git

#haskell syntax highlighting
cd ~/.vim/bundle
git clone https://github.com/neovimhaskell/haskell-vim.git

#sensible
cd ~/.vim/bundle
git clone git://github.com/tpope/vim-sensible.git

# fugitive
cd ~/.vim/bundle
git clone https://github.com/tpope/vim-fugitive.git
vim -u NONE -c "helptags vim-fugitive/doc" -c q


mv .vimrc ~
