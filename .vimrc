set nocompatible
filetype off

"Vundle Setup 
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

Plugin 'airblade/vim-gitgutter'
Plugin 'VundleVim/Vundle.vim'
Plugin 'sjl/badwolf'
Plugin 'ctrlpvim/ctrlp.vim'
Plugin 'itchyny/lightline.vim'

call vundle#end()
"Vundle End

execute pathogen#infect()
syntax on
filetype plugin indent on
set number
set mouse=a
set backspace=indent,eol,start

set expandtab

set tabstop=4

set softtabstop=4

set shiftwidth=4

set laststatus=2

set fileencoding=utf-8

set encoding=utf-8

set modeline

set t_Co=256

"Hasksell Setup

let g:haskell_enable_quantification = 1   " to enable highlighting of `forall`
let g:haskell_enable_recursivedo = 1      " to enable highlighting of `mdo` and `rec`
let g:haskell_enable_arrowsyntax = 1      " to enable highlighting of `proc`
let g:haskell_enable_pattern_synonyms = 1 " to enable highlighting of `pattern`
let g:haskell_enable_typeroles = 1        " to enable highlighting of type roles
let g:haskell_enable_static_pointers = 1  " to enable highlighting of `static`
let g:haskell_backpack = 1
